" Organization following up the :options sections

" {{{ 1. Important
set nocompatible

" {{{ 1.1 Vundler specifics

call plug#begin('~/.vim/plugged')
Plug 'gmarik/Vundle.vim'

" Autoclose
Plug 'jiangmiao/auto-pairs'

" a.vim
Plug 'vim-scripts/a.vim'

" colorschemes
Plug 'flazz/vim-colorschemes'

" Fugitive
Plug 'tpope/vim-fugitive'
" GV
Plug 'junegunn/gv.vim'

" Merginal (Fugitive plugin for branch management)
Plug 'idanarye/vim-merginal'

" Gundo
Plug 'sjl/gundo.vim'

" Jedi-vim
Plug 'davidhalter/jedi-vim'

" Narrow region
Plug 'chrisbra/NrrwRgn'

" NERDCommenter
Plug 'scrooloose/nerdcommenter'

" Numbers
Plug 'myusuf3/numbers.vim'

" Surround
Plug 'tpope/vim-surround'

" Syntastic
Plug 'scrooloose/syntastic'

" Tagbar
Plug 'majutsushi/tagbar'

" Ultisnips
Plug 'SirVer/ultisnips'

" Ctrlp
Plug 'ctrlpvim/ctrlp.vim'

" NERDTree
Plug 'scrooloose/nerdtree'

" Vim Airline
Plug 'bling/vim-airline'

" Vim Snipets (for Ultisnips)
Plug 'honza/vim-snippets'

" Jellybeans colorscheme
Plug 'nanotech/jellybeans.vim'

" Vividchalk colorscheme
Plug 'tpope/vim-vividchalk'

" Tabularize
Plug 'godlygeek/tabular'

" Vim gitgutter
Plug 'airblade/vim-gitgutter'

" Bufexplorer
Plug 'jlanzarotta/bufexplorer'

" Bad wolf colorscheme
Plug 'sjl/badwolf'

" vim-ecliptic
Plug 'richsoni/vim-ecliptic'

" multiple cursors
Plug 'terryma/vim-multiple-cursors'

" Incsearch improved
Plug 'haya14busa/incsearch.vim'
"
" Grepper
Plug 'mhinz/vim-grepper'

" FlatColor colorscheme
Plug 'MaxSt/FlatColor'

" Targets (next text objects and more text objects)
Plug 'wellle/targets.vim'

" Dependency for Easytags
Plug 'xolox/vim-misc'
" Easytags
Plug 'xolox/vim-easytags'

" clever-f
Plug 'rhysd/clever-f.vim'

" vim-repeat
Plug 'tpope/vim-repeat'

" Matchup
Plug 'andymass/vim-matchup'

" YCM
Plug 'Valloric/YouCompleteMe', { 'do': './install.sh --clang-completer'  }

" Unimpaired
Plug 'tpope/vim-unimpaired'

" VIMagit
Plug 'jreybert/vimagit'

" Twiggy
Plug 'sodapopcan/vim-twiggy'

" Conflicted
Plug 'christoomey/vim-conflicted'

" NERDTree git plugin
Plug 'Xuyuanp/nerdtree-git-plugin'

" Colorscheme
Plug 'ajmwagar/vim-deus'

" FZF
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all'  }
Plug 'junegunn/fzf.vim'

" Custom text objects
Plug 'kana/vim-textobj-user'
Plug 'kana/vim-textobj-function'
Plug 'kana/vim-textobj-lastpat'
Plug 'kana/vim-textobj-line'

Plug 'neomake/neomake'

Plug 'shumphrey/fugitive-gitlab.vim'
" Bunch of unix-like commands for vim
Plug 'tpope/vim-eunuch'
Plug 'gu-fan/simpleterm.vim'

call plug#end()
filetype plugin indent on

" }}}

set t_Co=256
set rtp+=~/.fzf

if has("gui_running")
    set guioptions-=T
endif

set formatoptions=croq

" Invisible Buffers
set hidden

set timeoutlen=200

" Let netrw act as tree view by default
let g:netrw_liststyle=3
" Enable preview mode, use it with P
let g:netrw_preview = 1
" Open vertial splits to the right
let g:netrw_altv = 1

" }}}

" {{{ 2. Moving around, searching and patterns
"nnoremap / /\v
"vnoremap / /\v
map / <Plug>(incsearch-forward)
map ? <Plug>(incsearch-backward)
map g/ <Plug>(incsearch-stay)

let g:incsearch#magic = '\v'

let g:incsearch#auto_nohlsearch = 1
map n  <Plug>(incsearch-nohl-n)
map N  <Plug>(incsearch-nohl-N)
map *  <Plug>(incsearch-nohl-*)
map #  <Plug>(incsearch-nohl-#)
map g* <Plug>(incsearch-nohl-g*)
map g# <Plug>(incsearch-nohl-g#)

map oo <C-]>

set incsearch
set ignorecase smartcase

set hlsearch
" }}}

" {{{ 3.Tags
" }}}

" {{{ 4. Displaying text
set nowrap
" }}}

" {{{ 5. Syntax, highlighting and spelling
syntax enable
" }}}

" {{{ 6. Multiple windows
" }}}

" {{{ 7. Multiple tab pages
" }}}

" {{{ 8. Terminal
" }}}

" {{{ 9. Using the mouse
set mouse=a
if &term =~ '^screen'
    " tmux knows the extended mouse mode
    set ttymouse=xterm2
endif
" }}}

" {{{ 10. Printing
set ruler
set showmatch
set showmode
set number
set smartcase
set scrolloff=3
set mousehide

" Trailing whitspaces
set list
set listchars=tab:>\ ,trail:.,extends:#,nbsp:.

" Status line (don't really need this because of vim-airline)
set ls=2
"
set wildmenu
set wildmode=longest,full
set completeopt=menuone,preview
set wildignore+=*/tmp/*,*/target/*,*.zip,*.jar,*.class,*.pyc

set guicursor=i-c:hor1
set cursorline

" Warning column at 80
let &colorcolumn="80,120"
" }}}

" {{{ 11. Messages and info
" }}}

" {{{ 12. Selecting text
" }}}

" {{{ 13. Editing text
" }}}

" {{{ 14. Tabs and indenting
if version >= 700
  filetype plugin indent on
else
  filetype on
  filetype plugin on
  filetype indent on
endif

set autoindent

set backspace=indent,eol,start

set smartindent
set tabstop=4
set softtabstop=4
set shiftwidth=4
set expandtab
" }}}

" {{{ 15. Folding
set foldlevelstart=20
" }}}

" {{{ 16. Diff mode
" }}}

" {{{ 17. Mapping
let mapleader = ","

" ######## --<leader>. = alternate buffer
nnoremap <leader>. :b#<CR>

" ######## --<leader>ec = edit vimrc config
nnoremap <silent> <leader>ec :tabnew ~/.vimrc<CR>

" ######## --<leader>sc = source vimrc
nnoremap <silent> <leader>sc :source ~/.vimrc<CR>

" ######## --jj-- = <ESC>
imap jj <ESC>

" ######## --jj-- = <ESC>
nnoremap <space> za
nnoremap <leader>o zA
vnoremap <space> zf

" ######## --jj-- = <ESC>
vnoremap y myy`y
vnoremap Y myY`y

" ######## Movements with wrapped lines
nnoremap j gj
nnoremap k gk
nnoremap H ^
nnoremap L $
vnoremap L g_

" ######## -- C-[] = Movement between buffers
nnoremap <silent> <c-s-[> :bp<cr>
nnoremap <silent> <c-s-]> :bn<cr>

" Disable the arrows (need a workaround to disable this with a mapping)
"nnoremap <up> <nop>
"nnoremap <down> <nop>
"nnoremap <left> <nop>
"nnoremap <right> <nop>
"inoremap <up> <nop>
"inoremap <down> <nop>
"inoremap <left> <nop>
"inoremap <right> <nop>

" ######## --C-l = clean the matched words
nnoremap <silent> <c-l> :noh<CR>

" ######## Formatting
nnoremap Q qpip
vnoremap Q qp

" ######## Curson in place when joining lines
nnoremap J mzvipJ`z

" ######## Keep the curson in center when searching for next occurrence
nnoremap n nzzzv
nnoremap N Nzzzv

function! WrapRelativeMotion(key, ...)
    let vis_sel=""
    if a:0
        let vis_sel="gv"
    endif
    if &wrap
        execute "normal!" vis_sel . "g" . a:key
    else
        execute "normal!" vis_sel . a:key
    endif
endfunction

noremap <silent> $ :call WrapRelativeMotion("$")<CR>
noremap <silent> <End> :call WrapRelativeMotion("$")<CR>
noremap <silent> 0 :call WrapRelativeMotion("0")<CR>
noremap <silent> <Home> :call WrapRelativeMotion("0")<CR>
noremap <silent> ^ :call WrapRelativeMotion("^")<CR>

vnoremap <silent> $ :<C-U>call WrapRelativeMotion("$", 1)<CR>
vnoremap <silent> <End> :<C-U>call WrapRelativeMotion("$", 1)<CR>
vnoremap <silent> 0 :<C-U>call WrapRelativeMotion("0", 1)<CR>
vnoremap <silent> <Home> :<C-U>call WrapRelativeMotion("0", 1)<CR>
vnoremap <silent> ^ :<C-U>call WrapRelativeMotion("^", 1)<CR>

" ######## Fix copy till end feature
nnoremap <silent> Y y$

" ######## --<leader>+motion-- Window jumping
nmap <silent> <leader>j <C-W>j
nmap <silent> <leader>k <C-W>k
nmap <silent> <leader>l <C-W>l
nmap <silent> <leader>h <C-W>h

" ######## --<leader>+[v/s]-- Window duplicate
nmap <leader>v <C-W>v
nmap <leader>s <C-W>s

" ######## --<leader>+q-- Window close
nmap <leader>q <C-W>c

" ######## --<leader>+p-- Toggle paste mode
nnoremap <leader>p :set paste!<CR>

" ######## --<visual>+[</>]-- Window inifite resize
vnoremap < <gv
vnoremap > >gv

" ######## --<c-p>-- File search (fuzzy search)
"nnoremap <C-P> :CtrlPMixed<CR>
let g:ctrlp_cmd = 'CtrlPMixed'

" ######## --F2-- File tree
nmap <silent> <F2> :NERDTreeToggle<CR>

" ######## --F3-- Find file in tree
nmap <silent> <F3> :NERDTreeFind<CR>

" ######## --F5-- Gundo
nnoremap <F5> :GundoToggle<CR>

" ######## --F6-- Listar buffers
nnoremap <F6> :BufExplorerHorizontalSplit<CR>
nnoremap <leader>b :Buffers<CR>

" ######## --F10 (no auto-close) F11 (auto-close)-- Tagbar
nmap <leader>tt :TagbarToggle<CR>
nmap <F10> :TagbarToggle<CR>
nmap <F11> :TagbarOpenAutoClose<CR>
let g:tagbar_autofocus = 1
let g:tagbar_sort = 0

" ######## --F12-- a.vim
nmap <silent> <F12> :A<CR>
" }}}

" {{{ 18. Reading and wiring files
" }}}

" {{{ 19. The swap file
set backup
set writebackup

set backupdir=~/tmp,/var/tmp,/tmp
set backupskip=/tmp/*,/private/tmp/*
set directory=~/tmp,/var/tmp,/tmp
" }}}

" {{{ 20. Command line editing
" }}}

" {{{ 21. Executing external commands
" }}}

" {{{ 22. Running make and jumping to errors
" }}}

" {{{ 23. Language specific
" }}}

" {{{ 24. Multi-byte characters
" }}}

" {{{ 25. Colors and fonts
" colorscheme flatcolor
"colorscheme jellybeans
"colorscheme zenburn
colorscheme deus
hi CursorLine ctermbg=None


hi Search ctermbg=Yellow guibg=Yellow
hi Search ctermfg=Black guifg=Black

set guifont=Inconsolata\ 12

let g:indent_guides_auto_colors = 1
let g:indent_guides_start_level = 2
let g:indent_guides_guide_size = 1
" }}}


" {{{ 26. Various

if has("autocmd") && exists("+omnifunc")
    autocmd Filetype *
                \if &omnifunc == "" |
                \setlocal omnifunc=syntaxcomplete#Complete |
                \endif
endif


" Jedi select first result
let g:jedi#popup_select_first = 0
let g:jedi#auto_vim_configuration = 1

" Syntastic checkers
let g:syntastic_python_checkers = ['flake8']
let g:syntastic_cpp_checkers = ['verapp', 'cppcheck']
let g:syntastic_cpp_verapp_exec = 'vera++'

let g:syntastic_cppcheck_config_file = '~/.cppcheck'
let g:syntastic_verapp_config_file = '~/.vera'

let g:syntastic_check_on_open = 1

" Adding NERDCommenter custom strings for Python to comply with PEP
let s:nerdcommenterDelimiters = { 'python': {"left": "# "} }

if exists("g:NERDCustomDelimiters")
  call extend(g:NERDCustomDelimiters, s:nerdcommenterDelimiters)
else
  let g:NERDCustomDelimiters = s:nerdcommenterDelimiters
endif

let g:loaded_golden_ratio = 1

let g:UltiSnipsExpandTrigger="<c-j>"
let g:UltiSnipsJumpForwardTrigger="<c-j>"
let g:UltiSnipsJumpBackwardTrigger="<c-k>"

let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#show_buffers = 1

set shell=/bin/bash

let NERDTreeIgnore = ['\.pyc$']

" Enable async update for easytags
let g:easytags_async = 1

" Bookmarks for startify
let g:startify_bookmarks = [ {'v': '~/.vimrc'} ]


" Operators for Grepper
runtime plugin/grepper.vim
nmap gs <Plug>(GrepperOperator)
xmap gs <Plug>(GrepperOperator)
let g:grepper.tools = ['git', 'grep']

nnoremap <leader>cc :cclose<CR>

let g:ycm_autoclose_preview_window_after_completion=1
let g:ycm_key_list_select_completion = ['<TAB>', '<Down>']
let g:ycm_key_list_previous_completion = ['<Up>']
let g:ycm_show_diagnostics_ui = 0
nnoremap <leader>gt :YcmCompleter GoTo<CR>
nnoremap <leader>fx :YcmCompleter FixIt<CR>

nmap <silent> <leader>ff :Files<CR>
nmap <silent> <leader>gf :GFiles<CR>
nmap <silent> <leader>lo :Commits<CR>

au! VimEnter * AddTabularPattern! spaces /\S\+;$/l1
vmap <silent> <leader>ts :Tabularize spaces<CR>

au! BufNewFile,BufRead SConscript set filetype=python
au! BufNewFile,BufRead SConstruct set filetype=python
au! BufNewFile,BufRead Jenkinsfile set filetype=groovy

command! BufOnly silent! execute "%bd|e#|bd#"

" }}}
