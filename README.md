Vim installation directory for alex952
======

New installation
-----

    cd ~
    git clone https://alex952@bitbucket.org/alex952/viminstance.git ~/.vim
    ln -s ~/.vim/.vimrc
    vim +PlugInstall

Update all plugins installed
-----

    vim +PlugUpdate
